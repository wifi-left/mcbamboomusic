# Bamboo Music in Minecraft 插件

一个全服点歌插件，本项目是 AllMusic 的第三方分支。

![GIF.gif](img%2FGIF.gif)

注：
1. 需要配合[客户端Mod](https://gitlab.com/wifi-left/AllMusic_Client)使用（AllMusic客户端也可以）
2. 请在设置中配置API地址，API搭建方法、要求请见 [#设置api地址](#设置api地址)

[下载服务端模组/插件 on Modrinth](https://modrinth.com/plugin/allmusic-localapi/versions)

[客户端MOD下载地址](https://www.123pan.com/s/oButVv-hvI2.html)

支持的服务器
- LoliServer
- CatServer
- CraftBukkit
- Spigot
- Paper
- Bungeecord
- Velocity
- KCauldron
- Uranium
- Thermos
- Mirai

## 使用方法
1. 安装AllMusic插件  
   复制`AllMusic-2.18.0-all.jar`到你的`plugins`文件夹  
   重启过服务器
2. 安装客户端mod  
   目前只支持
   `forge:1.20`  
   `fabric:1.20`  
   复制`AllMusic-版本号.jar`到客户端的`mods`文件夹  
   fabric同理

## 设置API地址
更改 `UrlRoot` 为你的 API 地址。

网页端API请参考[Bamboo Music Manager - 新增自建 API](https://gitlab.com/wifi-left/bamboo-music-website/-/tree/master#%E6%96%B0%E5%A2%9E%E8%87%AA%E5%BB%BA-api)

API地址还需要满足：

当访问参数`type=status`时，`response code`为`200`（内容任意）

## API
您需要配置API，网页端API请参考[Bamboo Music Manager](https://gitlab.com/wifi-left/bamboo-music-website) 中的 [local.php](https://gitlab.com/wifi-left/bamboo-music-website/-/blob/master/apis/local/local.php)

## PAPI变量  
> %allmusic_now_music_name% 歌曲名字  
> %allmusic_now_music_al% 歌曲专辑  
> %allmusic_now_music_alia% 歌曲原曲  
> %allmusic_now_music_author% 歌曲作者  
> %allmusic_now_music_call% 点歌人  
> %allmusic_now_music_info% 歌曲所有信息  
> %allmusic_list_size% 队列大小  
> %allmusic_music_list% 队列歌曲  
> %allmusic_lyric% 歌词  
> %allmusic_tlyric% 翻译的歌词
> %allmusic_klyric% KTV歌词  

### 更新日志

```
插件：
API修改。
```
