package coloryr.allmusic.core.objs.message;
import coloryr.allmusic.core.AllMusic;

public class CommandObj {
    public String Error;
    public String NoPer;

    public boolean check() {
        if (Error == null)
            return true;
        return NoPer == null;
    }

    public void init() {
        Error = AllMusic.getConfig().MessagePrefix + "§c参数错误，请输入/music help获取帮助";
        NoPer = AllMusic.getConfig().MessagePrefix + "§c你没有权限点歌";
    }

    public static CommandObj make() {
        CommandObj obj = new CommandObj();
        obj.init();

        return obj;
    }
}
