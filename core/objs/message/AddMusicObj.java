package coloryr.allmusic.core.objs.message;
import coloryr.allmusic.core.AllMusic;

public class AddMusicObj {
    public String ListFull;
    public String PlayerListFull;
    public String BanMusic;
    public String ExistMusic;
    public String Success;
    public String NoPlayer;
    public String NoID;
    public String Cancel;
    public String TimeOut;

    public boolean check() {
        if (ListFull == null)
            return true;
        if (PlayerListFull == null)
            return true;
        if (BanMusic == null)
            return true;
        if (ExistMusic == null)
            return true;
        if (Success == null)
            return true;
        if (NoPlayer == null)
            return true;
        if (NoID == null)
            return true;
        if (Cancel == null)
            return true;
        return TimeOut == null;
    }

    public void init(){
        ListFull = AllMusic.getConfig().MessagePrefix + "§c错误，队列已满";
        BanMusic = AllMusic.getConfig().MessagePrefix + "§c错误，这首歌被禁点了";
        ExistMusic = AllMusic.getConfig().MessagePrefix + "§c错误，这首歌已经存在了";
        Success = AllMusic.getConfig().MessagePrefix + "§2点歌成功";
        NoPlayer = AllMusic.getConfig().MessagePrefix + "§c没有播放的玩家";
        NoID = AllMusic.getConfig().MessagePrefix + "§c错误，请输入歌曲数字ID";
        Cancel = AllMusic.getConfig().MessagePrefix + "§e点歌被取消";
        TimeOut = AllMusic.getConfig().MessagePrefix + "§e点歌被取消，音乐长度过长";
        PlayerListFull = AllMusic.getConfig().MessagePrefix + "§c错误，你在列表中点的歌曲已达上限";
    }

    public static AddMusicObj make() {
        AddMusicObj obj = new AddMusicObj();
        obj.init();

        return obj;
    }
}
