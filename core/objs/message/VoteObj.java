package coloryr.allmusic.core.objs.message;
import coloryr.allmusic.core.AllMusic;

public class VoteObj {
    public String NoPermission;
    public String DoVote;
    public String BQ;
    public String Agree;
    public String BQAgree;
    public String ARAgree;
    public String TimeOut;
    public String Do;

    public boolean check() {
        if (NoPermission == null)
            return true;
        if (DoVote == null)
            return true;
        if (BQ == null)
            return true;
        if (Agree == null)
            return true;
        if (BQAgree == null)
            return true;
        if (ARAgree == null)
            return true;
        if (TimeOut == null)
            return true;
        return Do == null;
    }

    public void init(){
        NoPermission = AllMusic.getConfig().MessagePrefix + "§c你没有权限切歌";
        DoVote = AllMusic.getConfig().MessagePrefix + "§e已发起切歌投票";
        BQ = AllMusic.getConfig().MessagePrefix + "§e%PlayerName%发起了切歌投票，%Time%秒后结束，输入/music vote 同意切歌。";
        Agree = AllMusic.getConfig().MessagePrefix + "§e你同意切歌";
        BQAgree = AllMusic.getConfig().MessagePrefix + "§e%PlayerName%同意切歌，共有%Count%名玩家同意切歌。";
        ARAgree = AllMusic.getConfig().MessagePrefix + "§e你已申请切歌";
        TimeOut = AllMusic.getConfig().MessagePrefix + "§e切歌时间结束";
        Do = AllMusic.getConfig().MessagePrefix + "§e已切歌";
    }

    public static VoteObj make() {
        VoteObj obj = new VoteObj();
        obj.init();

        return obj;
    }
}
