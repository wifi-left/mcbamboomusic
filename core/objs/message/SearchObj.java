package coloryr.allmusic.core.objs.message;
import coloryr.allmusic.core.AllMusic;

public class SearchObj {
    public String NoPer;
    public String CantSearch;
    public String Res;
    public String NoSearch;
    public String ErrorNum;
    public String Chose;
    public String CantNext;
    public String CantLast;
    public String StartSearch;

    public boolean check() {
        if (NoPer == null)
            return true;
        if (CantSearch == null)
            return true;
        if (Res == null)
            return true;
        if (NoSearch == null)
            return true;
        if (ErrorNum == null)
            return true;
        if (Chose == null)
            return true;
        if (CantNext == null)
            return true;
        if (CantLast == null)
            return true;
        return StartSearch == null;
    }

    public void init(){
        StartSearch = AllMusic.getConfig().MessagePrefix + "§e正在排队搜歌";
        NoPer = AllMusic.getConfig().MessagePrefix + "§c你没有权限搜歌";
        CantSearch = AllMusic.getConfig().MessagePrefix + "§c无法搜索歌曲：%Music%";
        Res = AllMusic.getConfig().MessagePrefix + "§e搜索结果";
        NoSearch = AllMusic.getConfig().MessagePrefix + "§c你没有搜索音乐";
        ErrorNum = AllMusic.getConfig().MessagePrefix + "§c请输入正确的序号";
        Chose = AllMusic.getConfig().MessagePrefix + "§e你选择了序号%Num%";
        CantNext = AllMusic.getConfig().MessagePrefix + "§c无法下一页";
        CantLast = AllMusic.getConfig().MessagePrefix + "§c无法上一页";
    }

    public static SearchObj make() {
        SearchObj obj = new SearchObj();
        obj.init();

        return obj;
    }
}
