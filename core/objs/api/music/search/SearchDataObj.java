package coloryr.allmusic.core.objs.api.music.search;

import java.util.List;

public class SearchDataObj {
    private result data;

    public boolean isOk() {
        return data != null && data.getSongs() != null;
    }

    public List<songs> getResult() {
        return data.getSongs();
    }
}

class result {
    private List<songs> list;

    public List<songs> getSongs() {
        return list;
    }
}