package coloryr.allmusic.core.objs.api.music.list;

import java.util.ArrayList;
import java.util.List;

public class DataObj {
    private playlist data;

    public List<String> getPlaylist() {
        List<String> list = new ArrayList<>();
        for (track item : data.getTracks()) {
            list.add(item.getId());
        }
        return list;
    }

    public String getName() {
        return data.getName();
    }
}

class track {
    private String id;

    public String getId() {
        return String.valueOf(id);
    }
}

class playlist {
    private List<track> list;
    private String name;

    public List<track> getTracks() {
        return list;
    }

    public String getName() {
        return name;
    }
}