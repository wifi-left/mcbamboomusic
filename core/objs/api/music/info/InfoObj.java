package coloryr.allmusic.core.objs.api.music.info;

public class InfoObj {
    private SongInfo data;

    public boolean isOk() {
        return (data != null);
    }

    public String getLrc() {
        return data.getLrc();
    }

    public String getName() {
        if (data == null)
            return "";
        return data.getSong().getName();
    }

    public String getAuthor() {
        if (data == null)
            return "";
        return data.getSong().getAr();
    }

    public String getAlia() {
        if (data == null)
            return "";
        return data.getSong().getAlia();
    }

    public String getAl() {
        return data.getSong().getAl();
    }

    public int getLength() {
        return 1;
    }

    public String getPicUrl() {
        if (data == null)
            return null;
        return data.getSong().getPicUrl();
    }
}

class SongInfo {
    private sinfo info;
    private String lrc;

    public sinfo getSong() {
        return info;
    }

    public String getLrc() {
        return lrc;
    }
}

class sinfo {
    private String name;
    private String artist;
    private String addition;
    private String album;
    private String pic;

    public String getName() {
        return name == null ? "" : name;
    }

    public String getAr() {
        return artist;
    }

    public String getAlia() {
        return addition;
    }

    public String getAl() {
        return album == null ? "" : album;
    }

    public String getPicUrl() {
        return pic;
    }
}