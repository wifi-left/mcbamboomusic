package coloryr.allmusic.core.music.play;

import coloryr.allmusic.core.AllMusic;
import coloryr.allmusic.core.objs.music.LyricItemObj;
import coloryr.allmusic.core.utils.Function;

import java.util.*;

public class LyricDo {
    private final Map<Integer, LyricItemObj> temp = new LinkedHashMap<>();
    public boolean isHave = false;
    public boolean isHaveK = false;
    public Map<Integer, String> kly = new LinkedHashMap<>();

    public Map<Integer, LyricItemObj> getTemp() {
        return temp;
    }

    public Map<Integer, String> getKLyric() {
        return kly;
    }

    /**
     * 检查歌词
     *
     * @param obj 歌词
     * @return 结果
     */
    public boolean check(String obj) {
        String[] lyric;
        obj = obj.replaceAll("\r\n", "\n");
        obj = obj.replaceAll("\n\r", "\n");
        obj = obj.replaceAll("\r", "\n");
        lyric = obj.split("\n");

        lrcReturn returns = getTime(lyric);
        Map<Integer, String> temp = returns.lyric;
        Map<Integer, String> temp1 = returns.tlyric;

        for (Map.Entry<Integer, String> item : temp.entrySet()) {
            if (!temp1.isEmpty()) {
                this.temp.put(item.getKey(), new LyricItemObj(item.getValue(), temp1.get(item.getKey())));
            } else {
                this.temp.put(item.getKey(), new LyricItemObj(item.getValue(), null));
            }
        }

        isHave = true;
        return false;
    }

    private class lrcReturn {
        public Map<Integer, String> lyric;
        public Map<Integer, String> tlyric;
    }

    /**
     * 将歌词转换成时间 字
     * 顺便将翻译弄进去
     * 
     * @param lyric 歌词
     * @return 结果
     */
    private lrcReturn getTime(String[] lyric) {
        Map<Integer, String> res = new LinkedHashMap<>();
        Map<Integer, String> tres = new LinkedHashMap<>();
        String min;
        String sec;
        String mil;
        int time;
        int milt;
        int lasttime = -1;
        for (int i = 0; i < lyric.length; i++) {
            String s = lyric[i];
            s = s.trim();
            if (s.startsWith("[")) {

                String temp = Function.getString(s, "[", "]");
                if (!temp.contains(".") || !temp.contains(":"))
                    continue;
                if (Function.countChar(temp, ':') > 1) {
                    String[] a = s.split(":");
                    min = a[0].substring(1);
                    sec = a[1];
                    mil = a[2];
                } else {
                    min = Function.getString(s, "[", ":");
                    sec = Function.getString(s, ":", ".");
                    mil = Function.getString(s, ".", "]");
                }
                String str = Function.getString(s, "]", null);

                if (s.replaceAll(" ", "") == "")
                    continue;
                if (!Function.isInteger(min))
                    continue;
                if (!Function.isInteger(sec))
                    continue;
                if (!Function.isInteger(mil))
                    continue;
                if (min.isEmpty() || sec.isEmpty() || mil.isEmpty())
                    continue;
                milt = Integer.parseInt(mil);
                if (mil.length() == 3) {
                    milt /= 10;
                }
                time = Integer.parseInt(min) * 60 * 1000 + Integer.parseInt(sec) * 1000 + milt * 10;

                if (time > 0 && time + AllMusic.getConfig().Delay > 0)
                    time += AllMusic.getConfig().Delay / 10 * 10;
                // 管你是啥反正上一句！
                res.put(time, str);
                tres.put(lasttime, str);
                lasttime = time;
            } else {
                // AllMusic.log.warning("Unexpected text: " + s);
            }
        }
        lrcReturn ret = new lrcReturn();
        ret.lyric = res;
        ret.tlyric = tres;
        // AllMusic.log.warning(ret.lyric)
        return ret;
    }
}
