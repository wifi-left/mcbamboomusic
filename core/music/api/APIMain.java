package coloryr.allmusic.core.music.api;

import java.net.URLEncoder;

// import coloryr.allmusic.AllMusicBC;
import coloryr.allmusic.core.AllMusic;
import coloryr.allmusic.core.music.play.LyricDo;
import coloryr.allmusic.core.music.play.LyricSave;
import coloryr.allmusic.core.objs.HttpResObj;
import coloryr.allmusic.core.objs.SearchMusicObj;
import coloryr.allmusic.core.objs.api.music.info.InfoObj;
import coloryr.allmusic.core.objs.api.music.list.DataObj;
// import coloryr.allmusic.core.objs.api.music.lyric.WLyricObj;
import coloryr.allmusic.core.objs.api.music.search.SearchDataObj;
import coloryr.allmusic.core.objs.api.music.search.songs;
import coloryr.allmusic.core.objs.music.SearchPageObj;
import coloryr.allmusic.core.objs.music.SongInfoObj;
import coloryr.allmusic.core.utils.Logs;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class APIMain {

    public int PlayNow = 0;
    public boolean isUpdata;

    public APIMain() {
        AllMusic.log.info(AllMusic.getConfig().MessagePrefix + "§e正在初始化...");
        HttpClientUtil.init();
        HttpResObj res = HttpClientUtil.get(AllMusic.getConfig().UrlRoot + "?type=status", "");
        if (res == null || !res.ok) {
            AllMusic.log.warning(AllMusic.getConfig().MessagePrefix + "§c初始化网络爬虫连接失败");
            AllMusic.init = false;
        }
    }

    /**
     * 获取手机验证码
     *
     * @param sender 发送者
     */
    public void sendCode(Object sender) {
        AllMusic.side.sendMessage(sender, AllMusic.getConfig().MessagePrefix + "§d此功能禁用");
    }

    /**
     * 登录
     *
     * @param sender 发送者
     * @param code   手机验证码
     */
    public void login(Object sender, String code) {
        AllMusic.side.sendMessage(sender, AllMusic.getConfig().MessagePrefix + "§c登录失败：功能禁用");
    }

    /**
     * 获取音乐详情
     *
     * @param id     音乐ID
     * @param player 用户名
     * @param isList 是否是空闲列表
     * @return 结果
     */
    private SongInfoObj getMusicDetail(String id, String player, boolean isList) {
        HttpResObj res = HttpClientUtil.get(AllMusic.getConfig().UrlRoot + "?type=info&value=" + id);
        if (res != null && res.ok) {
            InfoObj temp = AllMusic.gson.fromJson(res.data, InfoObj.class);
            if (temp.isOk()) {
                return new SongInfoObj(temp.getAuthor(), temp.getName(),
                        id, temp.getAlia(), player, temp.getAl(), false, temp.getLength(),
                        temp.getPicUrl(), false);
                // SongInfoObj(String Author, String Name, String ID, String Alia, String Call,
                // String Al,
                // boolean isList, int Length, String picUrl, boolean isTrial)
            }
        }
        return null;
    }

    /**
     * 获取音乐数据
     *
     * @param id     音乐ID
     * @param player 用户名
     * @param isList 是否是空闲列表
     * @return 结果
     */
    public SongInfoObj getMusic(String id, String player, boolean isList) {
        SongInfoObj info = getMusicDetail(id, player, isList);
        return info;
    }

    /**
     * 获取播放链接
     *
     * @param id 音乐ID
     * @return 结果
     */
    public String getPlayUrl(String id) {
        HttpResObj res = HttpClientUtil.get(AllMusic.getConfig().UrlRoot + "?type=url&value=" + id);
        if (res != null && res.ok) {
            try {
                return res.data;
            } catch (Exception e) {
                Logs.logWrite(res.data);
                AllMusic.log.warning("§c播放连接解析错误");
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 添加空闲歌单
     *
     * @param id     歌单id
     * @param sender 发送者
     */
    public void setList(String id, Object sender) {
        final Thread thread = new Thread(() -> {
            HttpResObj res = HttpClientUtil.get(AllMusic.getConfig().UrlRoot + "?type=album&value=" + id);
            if (res != null && res.ok)
                try {
                    isUpdata = true;
                    DataObj obj = AllMusic.gson.fromJson(res.data, DataObj.class);
                    AllMusic.getConfig().PlayList.addAll(obj.getPlaylist());
                    AllMusic.saveConfig();
                    AllMusic.side.sendMessaget(sender,
                            AllMusic.getMessage().MusicPlay.ListMusic.Get.replace("%ListName%", obj.getName()));
                } catch (Exception e) {
                    AllMusic.log.warning("§c歌曲列表获取错误");
                    e.printStackTrace();
                }
            isUpdata = false;
        }, "AllMusic_setList");
        thread.start();
    }

    /**
     * 获取歌词
     *
     * @param id 歌曲id
     * @return 结果
     */
    public LyricSave getLyric(String id) {
        LyricSave lyric = new LyricSave();
        HttpResObj res = HttpClientUtil.get(AllMusic.getConfig().UrlRoot + "?type=info&value=" + id);
        if (res != null && res.ok) {
            try {
                InfoObj obj = AllMusic.gson.fromJson(res.data, InfoObj.class);
                String lrc = obj.getLrc();
                LyricDo temp = new LyricDo();
                for (int times = 0; times < 1; times++) {
                    if (temp.check(lrc)) {
                        AllMusic.log.warning("§c歌词解析错误，解析第" + times + "次");
                    } else {
                        if (temp.isHave) {
                            lyric.setHaveLyric(AllMusic.getConfig().SendLyric);
                            lyric.setLyric(temp.getTemp());
                        }
                        return lyric;
                    }
                }
                AllMusic.log.warning("§c歌词解析失败");
            } catch (Exception e) {
                AllMusic.log.warning("§c歌词解析错误");
                e.printStackTrace();
            }
        }
        return lyric;
    }

    /**
     * 搜歌
     *
     * @param name      关键字
     * @param isDefault 是否是默认方式
     * @return 结果
     */
    public SearchPageObj search(String[] name, boolean isDefault) {
        if (AllMusic.init == false) {
            AllMusic.log.warning("§e温馨提示：§r初始化时访问API失败，请检查API是否正确，否则可能无法进行搜索等操作。");
        }
        List<SearchMusicObj> resData = new ArrayList<>();
        int maxpage;
        StringBuilder name1 = new StringBuilder();
        for (int a = isDefault ? 0 : 1; a < name.length; a++) {

            if (!name1.isEmpty()) {
                name1.append(" ");
            }
            name1.append(name[a]);

        }
        String queryString = "";
        try {
            queryString = URLEncoder.encode(name1.toString(), "UTF-8");
        } catch (Exception e) {
            queryString = name1.toString();
        }
        String getURL = AllMusic.getConfig().UrlRoot + "?type=search&value=" + queryString;
        HttpResObj res = HttpClientUtil.get(getURL);
        if (res != null && res.ok) {
            SearchDataObj obj = AllMusic.gson.fromJson(res.data, SearchDataObj.class);
            if (obj != null && obj.isOk()) {
                List<songs> res1 = obj.getResult();
                SearchMusicObj item;
                for (songs temp : res1) {
                    item = new SearchMusicObj(String.valueOf(temp.getId()), temp.getName(), temp.getArtists(),
                            temp.getAlbum());
                    resData.add(item);
                }
                maxpage = res1.size() / 10;
                if (res1.size() % 10 > 0) {
                    maxpage++;
                }
                if (res1.size() > 0 && !AllMusic.init) {
                    AllMusic.init = true;
                    AllMusic.log.info("API访问成功，已禁用API错误提示。");
                }
                return new SearchPageObj(resData, maxpage);
            } else {
                AllMusic.log.warning("§c歌曲搜索出现错误，请检查API是否正确。");
            }
        }
        return null;
    }

    /**
     * 获取空闲歌单的一首歌
     *
     * @return 结果
     */
    public String getListMusic() {
        if (!isUpdata && AllMusic.getConfig().PlayList.size() != 0) {
            String ID;
            if (AllMusic.getConfig().PlayListRandom) {
                if (AllMusic.getConfig().PlayList.size() == 0)
                    return null;
                else if (AllMusic.getConfig().PlayList.size() == 1)
                    return AllMusic.getConfig().PlayList.get(0);
                ID = AllMusic.getConfig().PlayList.get(new Random().nextInt(AllMusic.getConfig().PlayList.size()));
            } else {
                ID = AllMusic.getConfig().PlayList.get(PlayNow);
                if (PlayNow == AllMusic.getConfig().PlayList.size() - 1)
                    PlayNow = 0;
                else
                    PlayNow++;
            }
            return ID;
        }
        return null;
    }
}
