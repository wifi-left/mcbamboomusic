@echo off
call link.cmd
rd /s /q ".gradle"
mkdir ".gradle"
echo Building for Fabric
cd "fabric_1_20"
call gradlew clean build
cd ..
echo Building for Folia
cd "folia"
call gradlew clean build
cd ..
echo Building for Server Top...
cd "server_top"
call gradlew clean build
cd ..
xcopy "folia\build\libs\*.jar" "build\libs\" /Y
xcopy "fabric_1_20\build\libs\*.jar" "build\libs\" /Y
xcopy "server\build\libs\*.jar" "build\libs\" /Y
xcopy "server_top\build\libs\*.jar" "build\libs\" /Y
